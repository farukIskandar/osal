#include "osal_thread_impl.h"
#include <signal.h> // for pthread_kill()
#include <cstring>  // for memset()
#include <cstdio>
#include <errno.h>

#ifndef EOK
#define EOK (0)
#endif

#define check(A) \
    if (!(A))    \
    {            \
        break;   \
    }

static OSAL_Return_Code map_errno_2_osal_code(int e)
{
    OSAL_Return_Code return_code = OSAL_FAILED;
#define PROCESS_VAL(e, p) \
    case (e):             \
        return_code = p;  \
        break;

    switch (e)
    {
        PROCESS_VAL(EOK, OSAL_SUCCESS);
        PROCESS_VAL(EPERM, OSAL_PERMISSION_ERROR);
        PROCESS_VAL(ESRCH, OSAL_INVALID_THREAD_ID);
        PROCESS_VAL(EAGAIN, OSAL_RESOURCE_ERROR);
        PROCESS_VAL(EFAULT, OSAL_FAILED);
        PROCESS_VAL(EINVAL, OSAL_INVALID_THREAD_ATTRIBUTE);
        PROCESS_VAL(ENOTSUP, OSAL_INVALID_THREAD_SCHED_PARAM);

    default:
        break;
    }
#undef PROCESS_VAL
    return return_code;
}

OSAL_Return_Code OSAL_Thread::start(routine_t routine, void *routine_arg, OSALThreadPriority priority)
{
    OSAL_Return_Code return_code = OSAL_FAILED;
    last_set_priority = OSAL_THREAD_INVALID_PRIORITY;

    do
    {
        check(routine != NULL);
        pthread_attr_t *pAttr = NULL;
        struct sched_param schedParam;
        memset(&schedParam, 0, sizeof(schedParam));

        if (OSAL_THREAD_INHERITED_PRIORITY != priority)
        {
            pthread_attr_t tAttr;
            pthread_attr_init(&tAttr);
            pthread_attr_setschedpolicy(&tAttr, OS_SCHEDULING_POLICY);
            pthread_attr_getschedparam(&tAttr, &schedParam);

            int posix_priority = this->OS_bMapPriority(priority);

            schedParam.sched_priority = posix_priority;
            return_code = map_errno_2_osal_code(pthread_attr_setinheritsched(&tAttr, PTHREAD_EXPLICIT_SCHED));
            check(return_code == OSAL_SUCCESS);

            return_code = map_errno_2_osal_code(pthread_attr_setschedparam(&tAttr, &schedParam));
            check(return_code == OSAL_SUCCESS);
            pAttr = &tAttr;
        }

        return_code = map_errno_2_osal_code(pthread_create(&m_data, pAttr, routine, routine_arg));
        check(return_code == OSAL_SUCCESS);
        last_set_priority = priority;

    } while (0);

    return return_code;
}

int OSAL_Thread::join(void **value_ptr)
{
    return pthread_join(m_data, value_ptr);
}

int OSAL_Thread::detach()
{
    return pthread_detach(m_data);
}

int OSAL_Thread::cancel()
{
    return pthread_cancel(m_data);
}

int OSAL_Thread::kill(int signal)
{
    return pthread_kill(m_data, signal);
}

void OSAL_Thread::exit(void *value_ptr)
{
    pthread_exit(value_ptr);
}

OSAL_Return_Code OSAL_Thread::set_priority(OSALThreadPriority priority)
{
    OSAL_Return_Code return_code = OSAL_INVALID_THREAD_PRIORITY;

    do
    {
        check(priority != OSAL_THREAD_INVALID_PRIORITY);
        check(priority != last_set_priority);

        if (priority != OSAL_THREAD_INHERITED_PRIORITY)
        {
            return_code = map_errno_2_osal_code(pthread_setschedprio(m_data, OS_bMapPriority(priority)));
            check(return_code == OSAL_SUCCESS);
        }
        else if (last_set_priority != OSAL_THREAD_INHERITED_PRIORITY)
        {
            int policy = 0;
            struct sched_param schedParam;
            memset(&schedParam, 0, sizeof(schedParam));

            pthread_getschedparam(pthread_self(), &policy, &schedParam);
            return_code = map_errno_2_osal_code(pthread_setschedparam(m_data, policy, &schedParam));
            check(return_code == OSAL_SUCCESS);
        }

        last_set_priority = priority;

    } while (0);
    return return_code;
}

int OSAL_Thread::OS_bMapPriority(OSALThreadPriority priority)
{
    int return_priority = -1;

    switch (priority)
    {
    case OSAL_THREAD_INHERITED_PRIORITY:
        return_priority = -1;
        break;
    case OSAL_THREAD_LOWEST_PRIORITY:
        return_priority = sched_get_priority_min(OS_SCHEDULING_POLICY);
        break;
    case OSAL_THREAD_PRIORITY_LOW:
        return_priority = 2;
        break;
    case OSAL_THREAD_PRIORITY_MEDIUM:
        return_priority = 43;
        break;
    case OSAL_THREAD_PRIORITY_HIGH:
        return_priority = 84;
        break;
    case OSAL_THREAD_HIGHEST_PRIORITY:
        return_priority = sched_get_priority_max(OS_SCHEDULING_POLICY) / 2;
        break;
    case OSAL_THREAD_INVALID_PRIORITY:
    default:
        break;
    }
}