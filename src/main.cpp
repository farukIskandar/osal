#include <iostream>
#include "osal_thread.h"
#include <unistd.h>

using namespace std;

void func(void *arg)
{
    int i = 0;
    while (1)
    {
        cout << "Hello" << i++ << endl;
        sleep(1);
    }
}

int main()
{
    OSAL_Thread *thread = new OSAL_Thread("thread1");
    cout << thread->start((routine_t)func, NULL, OSAL_THREAD_LOWEST_PRIORITY) << endl;
    cout << thread->set_priority(OSAL_THREAD_HIGHEST_PRIORITY) << endl;
    thread->join(NULL);
    return 0;
}