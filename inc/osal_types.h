#ifndef OSAL_TYPES_H
#define OSAL_TYPES_H

typedef enum
{
    OSAL_SUCCESS = 0,                  /**< */
    OSAL_FAILED = 1,                   /**< */
    OSAL_INVALID_THREAD_ATTRIBUTE = 2, /**< */
    OSAL_INVALID_THREAD_SCHED_PARAM = 3,
    OSAL_PERMISSION_ERROR = 4,
    OSAL_RESOURCE_ERROR = 5,
    OSAL_INVALID_THREAD_ID = 6,
    OSAL_INVALID_THREAD_PRIORITY = 7,
    ReturnCode__Count = 8
} OSAL_Return_Code;

#endif // !OSAL_TYPES_H
