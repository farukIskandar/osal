#ifndef OSAL_THREAD_IMPL_H
#define OSAL_THREAD_IMPL_H

#include "osal_thread_intf.h"
#include <pthread.h>
#include <sched.h>

#define OS_SCHEDULING_POLICY SCHED_RR

class OSAL_Thread : public OSAL_Thread_Intf
{
public:
  OSAL_Thread(std::string name)
      : OSAL_Thread_Intf(name)
  {
    last_set_priority = OSAL_THREAD_INHERITED_PRIORITY;
  }
  virtual ~OSAL_Thread() {}

  OSAL_Return_Code start(routine_t routine, void *routine_arg, OSALThreadPriority priority = OSAL_THREAD_INHERITED_PRIORITY);
  int join(void **value_ptr);
  int detach();
  int cancel();
  int kill(int signal);
  void exit(void *value_ptr);
  OSAL_Return_Code set_priority(OSALThreadPriority priority);

private:
  int OS_bMapPriority(OSALThreadPriority priority);
  pthread_t m_data;
  OSALThreadPriority last_set_priority;
};

#endif // !OSAL_THREAD_IMPL_H