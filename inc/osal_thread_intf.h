#ifndef OSAL_THREAD_INTF
#define OSAL_THREAD_INTF

#include <string>
#include "osal_types.h"

typedef void *(*routine_t)(void *);

typedef enum
{
    OSAL_THREAD_INVALID_PRIORITY,
    OSAL_THREAD_INHERITED_PRIORITY,
    OSAL_THREAD_LOWEST_PRIORITY,
    OSAL_THREAD_PRIORITY_LOW,
    OSAL_THREAD_PRIORITY_MEDIUM,
    OSAL_THREAD_PRIORITY_HIGH,
    OSAL_THREAD_HIGHEST_PRIORITY
} OSALThreadPriority;

class OSAL_Thread_Intf
{
  public:
    virtual ~OSAL_Thread_Intf(){};
    virtual OSAL_Return_Code start(routine_t routine, void *routine_arg, OSALThreadPriority priority = OSAL_THREAD_INHERITED_PRIORITY) = 0;
    virtual int join(void **value_ptr) = 0;
    virtual int detach() = 0;
    virtual int cancel() = 0;
    virtual int kill(int signal) = 0;
    virtual void exit(void *value_ptr) = 0;
    virtual OSAL_Return_Code set_priority(OSALThreadPriority priority) = 0;

  protected:
    OSAL_Thread_Intf(std::string name)
        : m_name(name)
    {
    }
    virtual int OS_bMapPriority(OSALThreadPriority priority) = 0;
    std::string m_name;
};

#endif // !OSAL_THREAD_INTF